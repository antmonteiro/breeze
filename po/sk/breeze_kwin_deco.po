# translation of breeze_kwin_deco.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2014, 2015, 2016, 2017.
# Mthw <jari_45@hotmail.com>, 2018.
# Matej Mrenica <matejm98mthw@gmail.com>, 2019, 2021.
msgid ""
msgstr ""
"Project-Id-Version: breeze_kwin_deco\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-07 00:48+0000\n"
"PO-Revision-Date: 2021-07-20 17:42+0200\n"
"Last-Translator: Matej Mrenica <matejm98mthw@gmail.com>\n"
"Language-Team: Slovak <kde-i18n-doc@kde.org>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.07.80\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: config/breezeexceptionlistwidget.cpp:88
#, kde-format
msgid "New Exception - Breeze Settings"
msgstr "Nová výnimka - Nastavenia Vánok"

#: config/breezeexceptionlistwidget.cpp:136
#, kde-format
msgid "Edit Exception - Breeze Settings"
msgstr "Upraviť výnimku - Nastavenia Vánok"

#: config/breezeexceptionlistwidget.cpp:167
#, kde-format
msgid "Question - Breeze Settings"
msgstr "Otázka - Nastavenia Vánok"

#: config/breezeexceptionlistwidget.cpp:168
#, kde-format
msgid "Remove selected exception?"
msgstr "Odstrániť vybranú výnimku?"

#. i18n: ectx: property (text), widget (QPushButton, removeButton)
#: config/breezeexceptionlistwidget.cpp:170
#: config/ui/breezeexceptionlistwidget.ui:91
#, kde-format
msgid "Remove"
msgstr "Odstrániť"

#: config/breezeexceptionlistwidget.cpp:300
#, kde-format
msgid "Warning - Breeze Settings"
msgstr "Upozornenie - Nastavenia Vánok"

#: config/breezeexceptionlistwidget.cpp:300
#, kde-format
msgid "Regular Expression syntax is incorrect"
msgstr "Syntax regulárneho výrazu je nesprávna"

#: config/breezeexceptionmodel.cpp:17
#, kde-format
msgid "Exception Type"
msgstr "Typ výnimky"

#: config/breezeexceptionmodel.cpp:17
#, kde-format
msgid "Regular Expression"
msgstr "Regulárny výraz"

#. i18n: ectx: property (text), item, widget (QComboBox, exceptionType)
#: config/breezeexceptionmodel.cpp:36 config/ui/breezeexceptiondialog.ui:72
#, kde-format
msgid "Window Title"
msgstr "Titulok okna"

#. i18n: ectx: property (text), item, widget (QComboBox, exceptionType)
#: config/breezeexceptionmodel.cpp:40 config/ui/breezeexceptiondialog.ui:67
#, kde-format
msgid "Window Class Name"
msgstr "Názov triedy okna"

#: config/breezeexceptionmodel.cpp:55
#, kde-format
msgid "Enable/disable this exception"
msgstr "Povoliť/zakázať túto výnimku"

#. i18n: ectx: attribute (title), widget (QWidget, tab)
#: config/ui/breezeconfigurationui.ui:33
#, kde-format
msgid "General"
msgstr "Všeobecné"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: config/ui/breezeconfigurationui.ui:39
#, kde-format
msgid "Tit&le alignment:"
msgstr "Zarovnanie titulku:"

#. i18n: ectx: property (text), item, widget (QComboBox, titleAlignment)
#: config/ui/breezeconfigurationui.ui:53
#, kde-format
msgid "Left"
msgstr "Vľavo"

#. i18n: ectx: property (text), item, widget (QComboBox, titleAlignment)
#: config/ui/breezeconfigurationui.ui:58
#, kde-format
msgid "Center"
msgstr "Vycentrovať"

#. i18n: ectx: property (text), item, widget (QComboBox, titleAlignment)
#: config/ui/breezeconfigurationui.ui:63
#, kde-format
msgid "Center (Full Width)"
msgstr "Vycentrovať (plná šírka)"

#. i18n: ectx: property (text), item, widget (QComboBox, titleAlignment)
#: config/ui/breezeconfigurationui.ui:68
#, kde-format
msgid "Right"
msgstr "Vpravo"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: config/ui/breezeconfigurationui.ui:89
#, kde-format
msgid "B&utton size:"
msgstr "Veľkosť tlačidiel:"

#. i18n: ectx: property (text), item, widget (QComboBox, buttonSize)
#: config/ui/breezeconfigurationui.ui:103
#, kde-format
msgid "Tiny"
msgstr "Drobné"

#. i18n: ectx: property (text), item, widget (QComboBox, buttonSize)
#. i18n: ectx: property (text), item, widget (QComboBox, shadowSize)
#: config/ui/breezeconfigurationui.ui:108
#: config/ui/breezeconfigurationui.ui:221
#, kde-format
msgctxt "@item:inlistbox Button size:"
msgid "Small"
msgstr "Malé"

#. i18n: ectx: property (text), item, widget (QComboBox, buttonSize)
#. i18n: ectx: property (text), item, widget (QComboBox, shadowSize)
#: config/ui/breezeconfigurationui.ui:113
#: config/ui/breezeconfigurationui.ui:226
#, kde-format
msgctxt "@item:inlistbox Button size:"
msgid "Medium"
msgstr "Stredné"

#. i18n: ectx: property (text), item, widget (QComboBox, buttonSize)
#. i18n: ectx: property (text), item, widget (QComboBox, shadowSize)
#: config/ui/breezeconfigurationui.ui:118
#: config/ui/breezeconfigurationui.ui:231
#, kde-format
msgctxt "@item:inlistbox Button size:"
msgid "Large"
msgstr "Veľké"

#. i18n: ectx: property (text), item, widget (QComboBox, buttonSize)
#. i18n: ectx: property (text), item, widget (QComboBox, shadowSize)
#: config/ui/breezeconfigurationui.ui:123
#: config/ui/breezeconfigurationui.ui:236
#, kde-format
msgctxt "@item:inlistbox Button size:"
msgid "Very Large"
msgstr "Veľmi veľká"

#. i18n: ectx: property (text), widget (QCheckBox, outlineCloseButton)
#: config/ui/breezeconfigurationui.ui:131
#, kde-format
msgid "Draw a circle around close button"
msgstr "Kresliť kružnicu okolo tlačidla zatvorenia"

#. i18n: ectx: property (text), widget (QCheckBox, drawBorderOnMaximizedWindows)
#: config/ui/breezeconfigurationui.ui:138
#, fuzzy, kde-format
#| msgid "Display window borders for maximized windows"
msgid "Draw border on maximized and tiled windows"
msgstr "Zobraziť okraje okna pre maximalizované okná"

#. i18n: ectx: property (text), widget (QLabel, drawBorderOnMaximizedWindowsHelpLabel)
#: config/ui/breezeconfigurationui.ui:163
#, kde-format
msgid ""
"This setting will allow you to resize from window edges touching screen "
"edges. Scrollbars and window close buttons will shift positions and will not "
"touch screen edges."
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, drawBackgroundGradient)
#: config/ui/breezeconfigurationui.ui:175
#, kde-format
msgid "Draw titlebar background gradient"
msgstr "Kresliť prechod pozadia záhlavia"

#. i18n: ectx: attribute (title), widget (QWidget, tab_4)
#: config/ui/breezeconfigurationui.ui:196
#, kde-format
msgid "Shadows"
msgstr "Tiene"

#. i18n: ectx: property (text), widget (QLabel, label)
#: config/ui/breezeconfigurationui.ui:202
#, kde-format
msgid "Si&ze:"
msgstr "Veľkosť:"

#. i18n: ectx: property (text), item, widget (QComboBox, shadowSize)
#: config/ui/breezeconfigurationui.ui:216
#, kde-format
msgctxt "@item:inlistbox Button size:"
msgid "None"
msgstr "Žiadne"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: config/ui/breezeconfigurationui.ui:244
#, kde-format
msgctxt "strength of the shadow (from transparent to opaque)"
msgid "S&trength:"
msgstr "Sila:"

#. i18n: ectx: property (suffix), widget (QSpinBox, shadowStrength)
#: config/ui/breezeconfigurationui.ui:257
#, no-c-format, kde-format
msgid "%"
msgstr "%"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: config/ui/breezeconfigurationui.ui:283
#, kde-format
msgid "Color:"
msgstr "Farba:"

#. i18n: ectx: attribute (title), widget (QWidget, tab_3)
#: config/ui/breezeconfigurationui.ui:310
#, kde-format
msgid "Window-Specific Overrides"
msgstr "Špecifické nastavenia okna"

#. i18n: ectx: property (windowTitle), widget (QDialog, BreezeExceptionDialog)
#: config/ui/breezeexceptiondialog.ui:14
#, kde-format
msgid "Dialog"
msgstr "Dialóg"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: config/ui/breezeexceptiondialog.ui:20
#, kde-format
msgid "Window Identification"
msgstr "Identifikácia okna"

#. i18n: ectx: property (text), widget (QLabel, label)
#: config/ui/breezeexceptiondialog.ui:26
#, kde-format
msgid "&Matching window property: "
msgstr "Zodpovedajúca vlastnosť okna: "

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: config/ui/breezeexceptiondialog.ui:39
#, kde-format
msgid "Regular expression &to match: "
msgstr "Zodpovedajúci regulárny výraz: "

#. i18n: ectx: property (text), widget (QPushButton, detectDialogButton)
#: config/ui/breezeexceptiondialog.ui:52
#, fuzzy, kde-format
#| msgid "Detect Window Properties"
msgid "Detect Window Properties…"
msgstr "Zistiť vlastnosti okna"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_2)
#: config/ui/breezeexceptiondialog.ui:83
#, kde-format
msgid "Decoration Options"
msgstr "Možnosti dekorácie"

#. i18n: ectx: property (text), widget (QCheckBox, borderSizeCheckBox)
#: config/ui/breezeexceptiondialog.ui:89
#, kde-format
msgid "Border size:"
msgstr "Veľkosť okraja:"

#. i18n: ectx: property (text), widget (QCheckBox, hideTitleBar)
#: config/ui/breezeexceptiondialog.ui:96
#, kde-format
msgid "Hide window title bar"
msgstr "Skryť titulok okna"

#. i18n: ectx: property (text), item, widget (QComboBox, borderSizeComboBox)
#: config/ui/breezeexceptiondialog.ui:107
#, kde-format
msgctxt "@item:inlistbox Border size:"
msgid "No Border"
msgstr "Bez okraja"

#. i18n: ectx: property (text), item, widget (QComboBox, borderSizeComboBox)
#: config/ui/breezeexceptiondialog.ui:112
#, kde-format
msgctxt "@item:inlistbox Border size:"
msgid "No Side Borders"
msgstr "Bez bočného okraja"

#. i18n: ectx: property (text), item, widget (QComboBox, borderSizeComboBox)
#: config/ui/breezeexceptiondialog.ui:117
#, kde-format
msgctxt "@item:inlistbox Border size:"
msgid "Tiny"
msgstr "Drobná"

#. i18n: ectx: property (text), item, widget (QComboBox, borderSizeComboBox)
#: config/ui/breezeexceptiondialog.ui:122
#, kde-format
msgctxt "@item:inlistbox Border size:"
msgid "Normal"
msgstr "Normálne"

#. i18n: ectx: property (text), item, widget (QComboBox, borderSizeComboBox)
#: config/ui/breezeexceptiondialog.ui:127
#, kde-format
msgctxt "@item:inlistbox Border size:"
msgid "Large"
msgstr "Veľké"

#. i18n: ectx: property (text), item, widget (QComboBox, borderSizeComboBox)
#: config/ui/breezeexceptiondialog.ui:132
#, kde-format
msgctxt "@item:inlistbox Border size:"
msgid "Very Large"
msgstr "Veľmi veľká"

#. i18n: ectx: property (text), item, widget (QComboBox, borderSizeComboBox)
#: config/ui/breezeexceptiondialog.ui:137
#, kde-format
msgctxt "@item:inlistbox Border size:"
msgid "Huge"
msgstr "Obrovská"

#. i18n: ectx: property (text), item, widget (QComboBox, borderSizeComboBox)
#: config/ui/breezeexceptiondialog.ui:142
#, kde-format
msgctxt "@item:inlistbox Border size:"
msgid "Very Huge"
msgstr "Veľmi obrovská"

#. i18n: ectx: property (text), item, widget (QComboBox, borderSizeComboBox)
#: config/ui/breezeexceptiondialog.ui:147
#, kde-format
msgctxt "@item:inlistbox Border size:"
msgid "Oversized"
msgstr "Nadmerná"

#. i18n: ectx: property (text), widget (QPushButton, moveUpButton)
#: config/ui/breezeexceptionlistwidget.ui:70
#, kde-format
msgid "Move Up"
msgstr "Posunúť hore"

#. i18n: ectx: property (text), widget (QPushButton, moveDownButton)
#: config/ui/breezeexceptionlistwidget.ui:77
#, kde-format
msgid "Move Down"
msgstr "Posunúť dole"

#. i18n: ectx: property (text), widget (QPushButton, addButton)
#: config/ui/breezeexceptionlistwidget.ui:84
#, kde-format
msgid "Add"
msgstr "Pridať"

#. i18n: ectx: property (text), widget (QPushButton, editButton)
#: config/ui/breezeexceptionlistwidget.ui:98
#, kde-format
msgid "Edit"
msgstr "Upraviť"

#~ msgid "Add handle to resize windows with no border"
#~ msgstr "Pridať spracovač na zmenu veľkosti okien bez okraja"

#~ msgid "Information about Selected Window"
#~ msgstr "Informácie o vybranom okne"

#~ msgid "Class: "
#~ msgstr "Trieda: "

#~ msgid "Title: "
#~ msgstr "Titulok: "

#~ msgid "Window Property Selection"
#~ msgstr "Výber vlastnosti okna"

#~ msgid "Use window class (whole application)"
#~ msgstr "Použiť triedu okna (celá aplikácia)"

#~ msgid "Use window title"
#~ msgstr "Použiť titulok okna"

#~ msgid "Allow resizing maximized windows from window edges"
#~ msgstr "Povoliť zmenu veľkosti maximalizovaných okien pomocou okrajov okna"

#~ msgid "Draw separator under active window's titlebar"
#~ msgstr "Kresliť oddeľovač pod  záhlavím aktívneho okna"

#~ msgid "Animations"
#~ msgstr "Animácie"

#~ msgid "Anima&tions duration:"
#~ msgstr "Trvanie animácií:"

#~ msgid " ms"
#~ msgstr " ms"

#~ msgid "Enable animations"
#~ msgstr "Povoliť animácie"

#~ msgctxt "@item:inlistbox Button size:"
#~ msgid "Normal"
#~ msgstr "Normálne"

#~ msgid "px"
#~ msgstr "px"

#~ msgid "S&ize:"
#~ msgstr "Veľkosť:"

#~ msgid "Form"
#~ msgstr "Formulár"
